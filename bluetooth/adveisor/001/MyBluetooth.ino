#include <SoftwareSerial.h>

const int led = 8;
const byte rxPin = 4;
const byte txPin = 5;

// Create an instance of SoftwareSerial
SoftwareSerial mySerial = SoftwareSerial(rxPin, txPin);


void setup() {
  Serial.begin(9600);
  
  // put your setup code here, to run once:
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  pinMode(led, OUTPUT);

  mySerial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if ( mySerial.available() > 0 ) {
    char c = mySerial.read();
    Serial.write(c);
    if ( c == 'a' ) {
      digitalWrite(led, HIGH);
    } else {
      digitalWrite(led, LOW);
    }
  }
}
