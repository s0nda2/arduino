/**
 * I2C Scanner
 * 
 * Soni D.
 * 
 * Inspired from: http://playground.arduino.cc/Main/I2cScanner
 */
#include <Wire.h>

byte error;

void setup() {
  // Initiate Wire and join the I2C bus.
  Wire.begin();
  // Initialise and wait for serial port to open.
  Serial.begin(9600);
  while (!Serial); // Wait for serial port (UART, USB) to connect.
  // Serial port is successfully connected.
  Serial.println("\n");
  Serial.println("********************************");
  Serial.println("*                              *");
  Serial.println("*       I2C Device Scanner     *");
  Serial.println("*                              *");
  Serial.println("********************************");
}

void loop() {
  byte nDevices;
  // Scanning for connected serial devices.
  Serial.println("\n--------------------------------");
  Serial.println("Scanning..");
  int address;
  for ( address = 0; address < 128; address++ ) {
    // Begins a transmission to the I2C device with the given
    // address between 0 and 127.
    Wire.beginTransmission(address);
    // The transmission to the (slave) I2C device will be ended by
    // calling Wire.endTransmission() that returns an error code
    // (byte) indicates the status of the transmission.
    error = Wire.endTransmission();
    // Evaluates error code.
    switch (error) {
      case 0:
        // The transmission was successful. Acknowlegement from 
        // I2C device received.
        Serial.print(" Address: 0x");
        if ( address < 16 ) {
          Serial.print("0");
        }
        Serial.println(address, HEX);
        nDevices++;
        break;
      /*
      case 1:
        Serial.print("\n Transmission failed at address ");
        Serial.println(address, HEX);
        Serial.println(" Error. Data too long to fit in transmit buffer.");
        break;
      case 2:
        Serial.print("\n Transmission failed at adress ");
        Serial.println(address, HEX);
        Serial.println(" Error: Received NACK on trasmit of address.");
        break;
      case 3:
        Serial.print("\n Transmission failed at adress ");
        Serial.println(address, HEX);
        Serial.println(" Error: Received NACK on trasmit of data.");
        break;
      */
      case 4: 
        // Other error.
        Serial.println(" Unknown error at address 0x");
        if ( address < 16 ) {
          Serial.print("0");
        }
        Serial.println(address, HEX);
        break;
      default:
        // Cases 1,2,3
        break;
    }
  }
  
  if ( nDevices > 0 ) {
    Serial.print(" Result: ");
    Serial.print(nDevices);
    Serial.println(" I2C device(s) found.");
    Serial.println(" Done.");
  } else {
    Serial.println(" No device could be found.");
  }
  
  delay(5000);
}
