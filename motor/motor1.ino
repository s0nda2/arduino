/**
 * Documentation about pin mapping of Arduino Motor Shield 
 * on Arduino.CC:
 * https://www.arduino.cc/en/Main/ArduinoMotorShieldR3
 * 
 * Tutorial about Arduino Motor Shield on Instructables.Com: 
 * http://www.instructables.com/id/Arduino-Motor-Shield-Tutorial/step4/One-Motor/
 * 
 * The shield's pins are summarised as follows:
 * 
 *      Function         ||  Channel A  |  Channel B  |
 *  ---------------------||-------------+-------------|
 *      Direction        ||     D12     |     D13     |
 *      PWM              ||     D3      |     D11     |
 *      Brake            ||     D9      |     D8      |
 *      Current Sensing  ||     A0      |     A1      |
 *          
 * where D12 indicates the digital pin 12 (similarly for D13, D3 etc.) 
 * and A0 indicates the analog pin 0.
 * The (digital) pins D3 and D11 are both marked with tilde sign (~) 
 * indicating PWM function. These pins are written with
 * 
 *         AnalogWrite(pin,val)
 *         
 * This function writes an value (8 Bit) between 0 and 255, instead of
 * just HIGH or LOW for digital signals.
 * 
 */
#define _WAIT_ 1000

int speed = 90;

void setup() {
  // put your setup code here, to run once:

  /* Initialise motor on channel A */
  pinMode(12, OUTPUT); // Set up direction pin of motor A.
  pinMode(9, OUTPUT); // Set up brake pin of motor A.

  /* Initialise motor on channel B */
  pinMode(13, OUTPUT); // Set up direction pin of motor B.
  pinMode(8, OUTPUT); // Set up brake pin of motor B.

  /* Initialise the motors' speed */
  analogWrite(3, speed); // Set motor A's speed (PWM) 0..255
  analogWrite(11, speed); // Set motor B's speed (PWM) 0..255
}


void loop() {
  // put your main code here, to run repeatedly:
  forward();
  delay(_WAIT_);

  turnLeft();
  delay(2000);
  
  backward();
  delay(_WAIT_);
  
  halt();
  delay(3000);
}


void halt() {
  digitalWrite(9, HIGH); // Activate motor A's brake. Stop it.
  analogWrite(3, 0); // Set motor A's speed (PWM) to zero. Stop it.

  digitalWrite(8, HIGH); // Activate motor B's brake. Stop it.
  analogWrite(11, 0); // Set motor B's speed (PWM) to zero. Stop it.
}


void forward() {
  digitalWrite(12, HIGH); // Set motor A's direction Forward.
  digitalWrite(9, LOW); // Deactivate motor A's brake.

  digitalWrite(13, LOW); // Set motor B's direction Backward.
  digitalWrite(8, LOW); // Deactivate motor B's brake.

  analogWrite(3, speed); // Set motor A's speed (PWM) 0..255
  analogWrite(11, speed); // Set motor B's speed (PWM) 0..255
}


void backward() {
  digitalWrite(12, LOW); // Set motor A's direction Forward.
  digitalWrite(9, LOW); // Deactivate motor A's brake.

  digitalWrite(13, HIGH); // Set motor B's direction Backward.
  digitalWrite(8, LOW); // Deactivate motor B's brake.

  analogWrite(3, speed); // Set motor A's speed (PWM) 0..255
  analogWrite(11, speed); // Set motor B's speed (PWM) 0..255
}

void turnLeft() {
  analogWrite(11, speed/2);
}

