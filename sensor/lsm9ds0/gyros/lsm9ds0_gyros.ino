/**
 * 
 * https://learn.sparkfun.com/tutorials/lsm9ds0-hookup-guide
 * 
 * http://engineering.stackexchange.com/questions/3348/calculating-pitch-yaw-and-roll-from-mag-acc-and-gyro-data
 * 
 */

#include <SPI.h>
#include <Wire.h>
#include <SFE_LSM9DS0.h>

// I2C communication
#define LSM9DS0_G 0x6B // Would be 0x6A if SD0_G is LOW
#define LSM9DS0_XM 0x1D // Would be 0x1E if SD0_XM is LOW

// Creates an instance of SFE_LSM9DS0
LSM9DS0 dof(MODE_I2C, LSM9DS0_G, LSM9DS0_XM);

#define PRINT_CALCULATED
//#define PRINT_RAW

#define PRINT_SPEED 500

float biasGyroX = 2.1;
float biasGyroY = 1.2;
float biasGyroZ = 3.68; 


void setup() {
  Serial.begin(115200);
  
  // Initialize the SFE_LSM9DS0
  uint16_t status = dof.begin();

  Serial.print("LSM9DS0 WHO_AM_I's returned: 0x");
  Serial.println(status, HEX);
  Serial.println("Should be 0x49D4\n");
}

void loop() {
  // put your main code here, to run repeatedly:
  printGyro();
  delay(PRINT_SPEED);
}

void printGyro() {
  dof.readGyro();
  Serial.print("G: ");
  
  #ifdef PRINT_CALCULATED
    float gyroX = ( abs(dof.calcGyro(dof.gx)) <= biasGyroX ) ? 0 : dof.calcGyro(dof.gx);
    float gyroY = ( abs(dof.calcGyro(dof.gy)) <= biasGyroY ) ? 0 : dof.calcGyro(dof.gy);
    float gyroZ = ( abs(dof.calcGyro(dof.gz)) <= biasGyroZ ) ? 0 : dof.calcGyro(dof.gz);

    /*
    Serial.print(gyroX);
    Serial.print(", ");
    Serial.print(gyroY);
    Serial.print(", ");
    Serial.print(gyroZ);
    */
    Serial.println(packData(gyroX,gyroY,gyroZ));
    
  #elif defined PRINT_RAW
    Serial.print(dof.gx,2);
    Serial.print(", ");
    Serial.print(dof.gy,2);
    Serial.print(", ");
    Serial.print(dof.gz,2);
  #endif
}

String packData(float gyroX, float gyroY, float gyroZ) {
  return String(gyroX) + String(", ")
         + String(gyroY) + String(", ")
         + String(gyroZ);
}

