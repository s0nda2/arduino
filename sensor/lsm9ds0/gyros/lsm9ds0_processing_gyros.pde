import processing.serial.*;

final static int baud = 11520;

int lf = 10; // Ascii for line feed
Serial myPort; // Create a Serial port instance
String myString;

float gx, gy, gz;

void setup() {
  myPort = new Serial(this, Serial.list()[0], baud);
  myPort.clear();
  // Buffer the incomming serial data until you get a newline (line feed) character.
  // Then, generate a serial event and call it.
  myPort.bufferUntil(lf);
  
  size(800, 600);
  background(0);
}

/**
 * This function is called whenever you get a newline (line feed) character in
 * the serial buffer.
 */
void serialEvent(Serial myPort) {
  if ( myPort.available() > 0 ) {
    myString = myPort.readStringUntil(lf);    
    if ( myString != null ) {
       //println(myString);
       // Process gyroscope data
       if ( myString.charAt(0) == 'G' ) {
         String[] sGyros = split(myString.substring(3), ",");
         if ( sGyros.length == 3 ) {
           gx = Float.parseFloat(sGyros[0]);
           gy = Float.parseFloat(trim(sGyros[1]));
           gz = Float.parseFloat(trim(sGyros[2]));
           println(gx + " | " + gy + " | " + gz);
         }
       }
    }
  }
}

int hPos = 0;
int tSize = 20;

void draw() {
  int vPos = height / 3;
  
  float _gx = map(gx, 0, 1023, 0, vPos);
  float _gy = map(gy, 0, 1023, 0, vPos);
  float _gz = map(gz, 0, 1023, 0, vPos);
  
  vPos /= 2;
  
  /*
   * Draw the gx area
   */
   stroke(245,124,1);
   line(hPos, vPos, hPos, vPos - _gx);
   
   textSize(tSize);
   fill(245,124,1);
   text("gyroX", 5, vPos - tSize);
   
   /*
   * Draw the gy area
   */
   stroke(125,252,5);
   line(hPos, 3*vPos, hPos, 3*vPos - _gy);
   
   fill(125,252,5);
   text("gyroY", 5, 3*vPos - tSize);
   
   /*
   * Draw the gz area
   */
   stroke(115,45,235);
   line(hPos, 5*vPos, hPos, 5*vPos - _gz);
   
   fill(115,45,235);
   text("gyroZ", 5, 5*vPos - tSize);
   
   // At the ending edge of each area, go back to the beginning
   if ( hPos > width ) {
     hPos = 0;
     background(0);
   } else {
     hPos++;
   }
}